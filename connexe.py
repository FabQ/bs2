# -*- coding: utf-8 -*-

from pathlib import Path
import os
import main
import numpy as np

class Connexe(main.Interaction_file) :

    # Attributes
    list=[]
    dic={}
    vertices=[]
    lcc=[]

    def __init__(self,filepath):
        self.list=self.read_interaction_file_list(filepath)
        self.dic=self.read_interaction_file_dict(filepath)
        self.vertices=self.get_vertices()
        self.lcc=self.computeCC()


    def extractCC(self,prot_name):
        """
        Return the protein list in the same connected component of the protein in argument
        @param : protein name
        @return : List of proteins in the same related component
        """
        CC_list = [prot_name]
        for key in CC_list:
            dic_list=self.dic.get(key)

            if dic_list is not None :
                # Perform the CC extraction only with element with interacting list
                for prot_cc in dic_list:
                    if prot_cc not in CC_list:
                        # Go through all interacting protein known with @prot_name :
                        # The interacting proteins and their self interacting protein
                        # are added in the output list
                        CC_list.append(prot_cc)
        return(CC_list)

    def computeCC(self):
        """
        See the connexe number associated with one protein
        @return : List : connexe number associated with the position of the protein in the list from the interaction file
        """
        list = self.vertices
        lcc_list = [0]*len(self.vertices)
        check_list = []
        ncc_int=1
        for CC_prot in list :
            # Check if a protein is already in the check_list
            if CC_prot not in check_list:
                # If not, the interacting proteins are appened in the lcc_list
                list_prot = self.extractCC(CC_prot)
                check_list = check_list + list_prot
                for prot in list_prot:
                    lcc_list[list.index(prot)]=ncc_int
                ncc_int=ncc_int+1
        return lcc_list

    def countCC(self):
        """
        Count the protein number in all connected component
        @param : protein name
        @return : List of proteins in the same related component
        """
        list = self.lcc
        array = np.unique(list, return_counts=True)
        #for i in array[0] :
            #print(" %s  %s" %(array[0][i-1],array[1][i-1]))
        return (array)


    def writeCC(self,fileName):
        """
        Write in the outfile @fileName.txt the length and the proteins in all connected component
        @param : fileName
        @countCC
        @return : List of proteins in the same related component
        """
        dict={}
        fill_list=[]
        array=self.countCC()
        # Go through the connexe number returned from the @countCC matrix
        for index in array[0]:
            fill_list=[]
            for y in range(len(self.vertices)):
                if index == self.lcc[y]:
                    fill_list.append(self.vertices[y])
                # Append all protein returned from the @countCC matrix
                dict[array[1][index-1]]=fill_list
        with open('%s.txt'%fileName,'w') as outfile :
            for k,v in dict.items():
                # Write lines in the outfile : outfile.txt
                outfile.write(str(k) + " " + str (v) + '\n')

    def density(self):
        """
        The density is the number of observed edges versus the theoric edges
        @return float : density number
        """
        return((2*self.count_edges())/(len(self.vertices)*(len(self.vertices)-1)))

    def clustering(self,prot):
        """
        Probability to have 2 proteins connected knowning that they share a connected protein
        @return : Float : probability number
        """
        triangle=float(0)
        if self.dic[prot] != None:
            for i in self.dic[prot]:
                if prot in self.dic.get(i) :
                    triangle = triangle+1
            pair = float(len(self.dic[prot])*(len(self.dic[prot])-1))/2
            print(triangle,pair)
        return float(triangle/pair)
