import numpy as np
import urllib.request
import pickle
from collections import Counter,OrderedDict
from matplotlib import pyplot as plt
from tqdm import tqdm


class Interaction_file:
    # Attributes
    list=[]
    dic={}

    def __init__(self,filepath):
        self.list=self.read_interaction_file_list(filepath)
        self.dic=self.read_interaction_file_dict(filepath)

    def read_interaction_file_dict(self,file):
        """
        Return a dictionary from an interaction file
        @param : interaction file
        @return : Dictionary : of elements from interaction file
        """
        interaction_file=open((file),"r")
        # Open the file in read only mode and store in fo variable
        # Read lines and store it to return
        # Store all interaction file line in a list
        next(interaction_file)
        lines = interaction_file.readlines()
        # Calling read_file function
        dictionary = {}

        # An empty dictionary
        for line in lines:
            # A loop to go through the list of lines returns by read_file function
            key,element = line.strip().split()
            # In a line, the first string will be the key
            # And the other will be the element
            dictionary.setdefault(key,[]).append(element)
            dictionary.setdefault(element,[]).append(key)
            # Dictionary is incremented by the key and the associated element
        interaction_file.close()
        return(dictionary)

    def read_interaction_file_list(self,file):
        """
        Return a list from an interaction file
        @param : interaction file
        @return :List : of elements from interaction file
        """
        #Open the parameter file
        with open(file, "r") as file_list:
            next(file_list)
            interaction_list=[]
            #we read the first line of the graph file which corresponds to the number of interactions
            for ligne_str in file_list.readlines():
                element_list=ligne_str.split()
                # each line create a couple
                # For each interaction file line, add in the list
                interaction_list.append(element_list)
        file_list.close()
        return(interaction_list)

    def read_interaction_file(self,file):
        """
        Return a tuple of a dictionnary and a list from an interaction file
        @param : interaction file
        @return : the dictionary @read_interaction_file_dict, then the list @read_interaction_file_list
        """
        couple_d_l_tuple = (self.read_interaction_file_dict(file), self.read_interaction_file_list(file))
        return couple_d_l_tuple

    def clean_interactome(self,fileName):
        """
        Have an interaction file without redundant or self interactions from the interaction file
        @outputs : a new "cleanfile.txt" in the directory
        @param : List of interaction file
        @return : List : of interaction file without self and redundant interactions
        """
        # Append elements from list in new list if there is no self or redundant interaction (e.g : 'A A', 'A B' and 'B A')
        clean_list=[]
        for element in self.list :
            A,B = element[0], element[1]
            if A != B:
                if [A,B] not in clean_list and [B,A] not in clean_list:
                        clean_list.append(A+" "+B)
        #outputs the result in a new interaction file : "cleanfile.txt"
        with open("%s.txt"%fileName,"w") as outfile:
            StrOfIntNumber=str(len(clean_list))
            outfile.write("%s \n"%StrOfIntNumber)
            outfile.write("\n".join(str(items) for items in clean_list))
        outfile.close()
        return(clean_list)

    def get_vertices(self):
        """
        Return a list of unique vertices in the interaction file
        @param : List of interaction file
        @return : List : of vertices
        """
        vertices_list=[]
        # Append elements from list in new list if there is no self or redundant interaction (e.g : 'A A', 'A B' and 'B A')
        for element in self.list :
            A,B = element[0], element[1]
            if A not in vertices_list :
                vertices_list.append(A)
            if B not in vertices_list :
                vertices_list.append(B)
        return vertices_list

    def count_vertices(self):
        """
        Return the number of vertices associated with the number of proteins in the interaction file
        @param : List of interaction file
        @return : Int : number of vertices
        """
        # Create the vertices list and add element if it's not already in the list
        return len(self.get_vertices())

    def count_edges(self):
        """
        Return the number of edges associated with the number of interactions in the interaction file
        @param : list of interaction file
        @return : Int : number of edges
        """
        edges_int=len(self.list)
        #print("Number of edges : %s" %(edges))
        return edges_int

    def get_degree(self,prot):
        """
        Return the number of interactions associated with a protein
        @param : Protein of interest
        @return : Int : number of interactions
        """
        return(len(self.dic[prot]))

    def get_max_degree(self):
        """
        Return the maximum number of interactions associated with a protein contained in an interaction file
        @get_vertices
        @clean_interactome
        @get_degree
        @param : interaction file, the protein of interest
        @return : String : The protein name and Int : the associated maximum number of interactions
        """
        max=0
        for protein in self.dic.keys():
            localmax=self.get_degree(protein)
            if localmax >max:
                max=localmax
                protein_name=protein
        #print("The protein %s has the highest degree : %s" %(protein_name,max))
        return (protein_name,max)
        #get_max_degree("/home/fabien/Desktop/M2_Bioinfo/BS2/toy_example.txt")

    def get_ave_degree(self):
        """
        Return the average number of interactions in an interaction file
        @get_vertices
        @get_degree
        @param : interaction file
        @return : Integer : Average number of interactions
        """
        summit_list=self.get_vertices()
        sum=0
        for element in summit_list:
            number=self.get_degree(element)
            sum+=number
        ave_int=float(sum)/float(len(summit_list))
        #print("The average number of degree is %s" %ave)
        return(ave_int)

    def count_degree(self,deg):
        """
        Count the protein number selected with a degree number
        @get_vertices
        @clean_interactome
        @get_degree
        @param : interaction file, Int degree
        @return : Integer : The associated maximum number of interactions
        """
        summit_list=self.get_vertices()
        prot_number_int=0
        for element in summit_list:
            number_int=self.get_degree(element)
            if number_int == deg :
                prot_number_int+=1
        #print("Number of proteins with %s degrees : %s" %(deg,prot_number))
        return(prot_number_int)

    def histogram_degree(self,dmin,dmax):
        """
        Draw an interaction histogram of the  interaction number with there protein number represented by stars selected in a range of degree
        @clean_interactome
        @count_degree
        @param : interaction file, degree minimum, degree maximum
        @return : Histogram
        """
        for number_int in range(dmin,dmax+1):
                prot_number_int=self.count_degree(number_int)
                print("%s " %(number_int),end= '')
                for i in range(0,prot_number_int):
                    print("*", end='')
                print("")

#data = Interaction_file("~/data/cleanFile.txt")
#print(data.histogram_degree(4,28))
