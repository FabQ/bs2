import csv

class Proteome:
    """
    Proteome class return name associated with UniprotID from a given Uniprotfile ID file
    """
    proteome=[]
    nameID={}
    idName={}
    dic={}

    def __init__(self,filepath,uniprotFilePath):
        self.proteome=self.__readProteome(uniprotFilePath)
        self.nameID=self.__readNameID(uniprotFilePath)
        self.idName=self.__readIDName(uniprotFilePath)
        self.dic = self.__read_interaction_file_dict(filepath)
        self.dict_graphe = self.x_link_domains(uniprotFilePath)

    def __read_interaction_file_dict(self,file):
        """
        Return a dictionary from an interaction file
        @param : interaction file
        @return : Dictionary : of elements from interaction file
        """
        interaction_file=open((file),"r")
        # Open the file in read only mode and store in fo variable
        # Read lines and store it to return
        # Store all interaction file line in a list
        next(interaction_file)
        lines = interaction_file.readlines()
        # Calling read_file function
        dictionary = {}

        # An empty dictionary
        for line in lines:
            # A loop to go through the list of lines returns by read_file function
            key,element = line.strip().split()
            # In a line, the first string will be the key
            # And the other will be the element
            dictionary.setdefault(key,[]).append(element)
            dictionary.setdefault(element,[]).append(key)
            # Dictionary is incremented by the key and the associated element
        interaction_file.close()
        return(dictionary)

    def __readProteome(self,filepath):
        """
        Read the proteome file and return it in a list
        @return : proteome in list
        """
        file=csv.reader(open(filepath),delimiter="\t")
        prot_list=[]
        for line in file:
            if line:
                prot_list.append(line[0])
        return prot_list

    def __readNameID(self,filepath):
        """
        Read the proteome file and create a dictionnary with the protein name associated with the UniprotID
        @return : dictionnary protein_name : uniprotID
        """
        file=csv.reader(open(filepath),delimiter="\t")
        nameID_dict={}
        for line in file:
            if line:
                nameID_dict.update({line[1]:line[0]})
        return nameID_dict

    def __readIDName(self,filepath):
        """
        Read the proteome file and create a dictionnary with  the UniprotID associated with the protein name
        @return : dictionnary uniprotID : protein_name
        """
        file=csv.reader(open(filepath),delimiter="\t")
        nameID_dict={}
        for line in file:
            if line:
                nameID_dict.update({line[0]:line[1]})
        return nameID_dict

    def xlink_Uniprot(self):
        """
        Create a dictionnary with Uniprot ID linked with the uniprot ID file path
        @key : protein name
        @elements : ['neighbor'] or ['UniprotID']
        @neighbor : return neighbor list of the key protein
        @read_interaction_file_dict
        @UniprotID : return the UniprotID associated with the protein name
        @domainComponent.Proteome.NameID

        @return : dictionary type
        """
        graph_dict={}
        for key in self.dic.keys():
            if key in self.nameID.keys():
                graph_dict[key] = {"UniprotID": self.nameID[key]}
                graph_dict[key]["Neighbor"] = self.dic[key]
        return graph_dict

    def get_protein_domains(self,protein):
        """
        Find the domain list of the @protein in entry from the webset Pfam associated with the protein name

        @return : list of domains associated with given protein
        """

        #If the protein is existing, create the domain list
        if protein in self.dic and "UniprotID" in self.dic[protein].keys():
            domain_list=[]
            #Search the UniprotID in the database associated with interactionFile
            uniprot_code = self.dict_graphe[protein]["UniprotID"]
            url="https://www.uniprot.org/uniprot/"+uniprot_code+".txt"
            #Open the Uniprot website page associated with protein name
            file = urllib.request.urlopen(url)
            for line in file:
                #Write the txt website page in text mode
                decoded_line = line.decode("utf-8")+'\n'
                split_decoded_line = decoded_line.split("; ")
                if "DR   Pfam" in split_decoded_line :
                    #Append the domain from Pfam and add coppy if the domain number is greater than 1
                    domain_list.append(split_decoded_line[2])
                    if int(split_decoded_line[3].split(".")[0]) > 1:
                        for i in range((int(split_decoded_line[3].split(".")[0]))-1):
                            domain_list.append(split_decoded_line[2])
            file.close()
            return(domain_list)

    def x_link_domains(self,filename):
        """
        Append the dictionnary interaction file created with @xlink_Uniprot function with domains from Pfam website with @get_protein_domains
        Due to the long computing time : the dictionnary dict_graphe is stored in uniprotDic.pkl file
        @xlink_Uniprot
        @get_protein_domains
        @return : The new dictionnary
        """
        #Create the file uniprot and store the current self.dict_graphe into
        with open('%s.pkl' %filename, 'wb') as f:  # Python 3: open(..., 'wb')
            for key in tqdm(self.dict_graphe.keys()):
                #For each protein, search the protein domain in pfam and append in the dictionary
                self.dict_graphe[key]["domains"] = self.__get_protein_domains(key)
            pickle.dump(self.dict_graphe,f)
            print("Dict import : done")
        f.close()
        return self.dict_graphe

    def uniprotID_Import(self):
        """
        How many proteins of the list have one or more uniprot code associated
        """
        count_protein_with_id=0
        count_protein_without_id=0
        for i in self.dict_graphe.values():
            if len(i.get('UniprotID')) != 0:
                count_protein_with_id+=1
            else :
                count_protein_without_id+=1
        return("Proteins with a Uniprot ID : %s \nProteins without Uniprot ID : %s \nProteins total : %s"%(count_protein_with_id,(count_protein_without_id+self.count_vertices()-count_protein_with_id),self.count_vertices()))

    def generate_weighted_cooccurrence_graph(self,fileName):
        """
        Creation of the object DomainGraph in dictionary with weight
        @return : dictionary
        @parameter name : fileName
        @value : Every domain present with the @key in a minimum of one protein
        """
        dict_return={}
        #Create the file co_occurence_graph and store the current self.dict_return into
        with open('%s.pkl' %fileName, 'wb') as f:  # Python 3: open(..., 'wb')
            # Go through the self.dict_graphe with a timer
            for protein in tqdm(self.dict_graphe.values()):
                # Store the domains in a list
                domains_list=list(protein.get('domains'))
                #domain_list greater with a minimum of 2 domains are used to increment the dict_return
                if len(domains_list) > 1 :
                    for domain in domains_list :
                        for other_domain in domains_list:
                            # Domain checking : already have a dict entity or not
                            if domains_list.index(other_domain) != domains_list.index(domain) and len(domain) > 0 and len(other_domain) > 0 :
                                if domain not in dict_return : dict_return[domain] = {other_domain:0}
                                if other_domain not in dict_return : dict_return[other_domain] = {domain:0}
                                if other_domain not in dict_return[domain] : dict_return[domain][other_domain] = 0
                                if domain not in dict_return[other_domain] : dict_return[other_domain][domain] = 0
                                # If the domain and the other_domain are both present, count in the weight score
                                dict_return[domain][other_domain] += 1
                                dict_return[other_domain][domain] += 1
            # Return the dict created in a file
            print(dict_return)
            pickle.dump(dict_return,f)
            print("Dict import on file : "+ name + ".pkl")
        f.close()
        return("Number of co-occurence domains : %s" %(sum(len(v) for v in iter(dict_return.values()))))
