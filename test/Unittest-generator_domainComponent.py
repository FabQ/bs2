#! /usr/bin/env python3

import os
import glob


humanProteome = "~/data/uniprot_proteome_Human.txt"
dataDir = "~/data"
solutionDir="~/solution"


generatedCode = """#! /usr/bin/env python3

import unittest
import main
import buildProteome

class testDomainComponentGenerated(unittest.TestCase):"""
usecaseFilePaths = glob.glob(dataDir + os.path.sep + "*.txt")

for currentUsecaseFilePath in usecaseFilePaths:
	solutionFilePath = currentUsecaseFilePath.replace(dataDir, solutionDir)
	if os.path.exists(solutionFilePath):
		with open(solutionFilePath) as solutionFile:
			currentSolution = solutionFile.read().strip()
			solutionFileName = currentUsecaseFilePath.replace(dataDir + os.path.sep, "").replace(".txt", "")
			generatedCode +="""

	def test_""" + solutionFileName + """(self):"""

		generatedCode +="""

		data = buildProteome.Proteome(""" + '"' + humanProteome  + '"' + """)"""

		readIDNameCurrentSolutionPath = solutionDir + "/"+ solutionFileName + "_idName.txt"
		if os.path.exists(readIDNameCurrentSolutionPath):
			with open(readIDNameCurrentSolutionPath) as solutionFile:
				currentSolution = solutionFile.read().strip()
				generatedCode +="""
		self.assertEqual(data.idName["E0YTP2"], """ +  '"' + currentSolution+ '"' + """, "Should be """ + currentSolution + """")"""

		readNameIDCurrentSolutionPath = solutionDir + "/" + solutionFileName + "_nameID.txt"
		if os.path.exists(readNameIDCurrentSolutionPath):
			with open(readNameIDCurrentSolutionPath) as solutionFile:
				currentSolution = solutionFile.read().strip()
				generatedCode +="""
		self.assertEqual(data.nameID["A0A5C2G978_HUMAN"], """ + '"' + currentSolution+ '"' +""", "Should be """ + currentSolution + """")"""


generatedCode += """

if __name__ == '__main__':
    unittest.main()
"""

with open("generatedTestDomainComponent.py", "w") as generatedFile:

	generatedFile.write(generatedCode)
