#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 21 19:11:01 2020

@author: fabien
"""

import read_interaction
import time
from pathlib import Path
import unittest

file1 = Path("~/data/toy_example.txt")
file2 = Path("~/data/Human_HighQuality.txt")
#print(read_interaction.read_interaction_file_list(file1))
#print(read_interaction.read_interaction_file_dict(file1))
#print(read_interaction.read_interaction_file_list(file2))
#print(read_interaction.read_interaction_file_dict(file2))

start_dict=time.time()
#print(start_dict)
read_interaction.read_interaction_file_dict(file1)
read_interaction.read_interaction_file_dict(file2)
print("The function read_interaction_file_dict is executed for file 1 and 2 is %s seconds " %(time.time()- start_dict))

start_list=time.time()
#print(start_list)
read_interaction.read_interaction_file_list(file1)
read_interaction.read_interaction_file_dict(file2)
print("The function read_interaction_file_list is executed for files 1 and 2 is %s seconds " %(time.time()- start_list))
#In the tests, create a list is faster than create a dictionary

#In my tests, create a list is faster than create a dictionary
class read_interaction_file(unittest.TestCase):
    def test_read_interac_file(self):
        """
        This test verify the good functioning of the read_interaction file
        """
        file = Path("/home/fabien/Desktop/M2_Bioinfo/BS2/toy_example.txt")
        #We store our test file in the variable file

        """
        We test if the read_file output is a list
        """
        test=read_interaction.read_file(file)
        t=type(test)
        self.assertEqual(t,list)

        """
        We test if the read_interaction_file_list output is a list
        """
        lis=read_interaction.read_interaction_file_list(file)
        t2=type(lis)
        #print(t2)
        self.assertEqual(t2,list)

        """
        We test if the read_interaction_file_dict output is a dictionary
        """
        dic=read_interaction.read_interaction_file_dict(file)
        t3=type(dic)
        self.assertEqual(t3,dict)

        """
        Is the first row contains the number of interactions in the file?
        It also tests if the file is not empty
        """
        TypeInt=(type(int(test[0])))
        #print(TypeInt)
        self.assertEqual(TypeInt,int)
        # We try to assert of the class of the first row
        #print("it's fine")

        NInt=int(test[0])
        LenList=int(len(test)-1)
        #print(LenList)
        self.assertEqual(NInt,LenList)
        # Then we test the equality of the number in the first row with all interactions

        """
        Is there the right number of column ?
        """
        column=read_interaction.read_interaction_file_list(file)[1]
        #print(type(column))
        data=len(column.split())
        self.assertEqual(data,2)
        #strip=len(column.strip())
        print("No problem occured")
unittest.main()

read_interaction_file(file1)
