#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 21 19:11:01 2020

@author: fabien
"""
import time
import unittest
import main
from pathlib import Path


class TestFunction(unittest.TestCase):
    def test_read_interaction_file(self):

        """This function applies several tests to check several features. """
        interaction_file = Path("Human_HighQuality.txt")

        """Tests if the output is a dictionary"""
        dict_type = type(main.read_interaction_file_dict(interaction_file))
        self.assertEqual(type(dict_type), dict)  # assertEqual() tests the equality between two parameters

        """Tests if the output is a list"""
        list_type = type(main.read_interaction_file_list(interaction_file))
        self.assertEqual(type(list_type), list)

        """Tests if the output is a list of tuples"""
        tuple_type = type(main.read_interaction_file(interaction_file))
        self.assertEqual(type(tuple_type), tuple)

    def timer_list_dic_file(self):
        start_dict=time.time()
        #print(start_dict)
        main.read_interaction_file_dict("toy_example.txt")
        main.read_interaction_file_dict("Human_HighQuality.txt")
        print("The function read_interaction_file_dict is executed for file 1 and 2 is %s seconds " %(time.time()- start_dict))

        start_list=time.time()
        #print(start_list)
        main.read_interaction_file_list("toy_example.txt")
        main.read_interaction_file_dict("Human_HighQuality.txt")
        print("The function read_interaction_file_list is executed for files 1 and 2 is %s seconds " %(time.time()- start_list))
            #In the tests, create a list is faster than create a dictionary

        """
        This function checks if the interaction file is in proper format to be read correctly.
        Returns True if correct format.
        1st case : the file doesn't have the 1st line of the interaction number
        2nd case : empty file
        3rd case : the interaction number on 1st line is not the right one
        4th case : file with a wrong number of columns
        5th case : file with only whitespaces
        """

    def is_interaction_file(File_Empty):

        with open(File_Empty, "r") as interaction_file:
            first_char = interaction_file.read(1)  # get the first character
            if not first_char:
                print("file is empty")  # first character is the empty string
            else:
                interaction_file.seek(0)  # first character wasn't empty, returns to the beginning of the file
        """
        Is the first row contains the number of interactions in the file?
        It also tests if the file is not empty
        """
        TypeInt=(type(int(test[0])))
        #print(TypeInt)
        self.assertEqual(TypeInt,int)
        # We try to assert of the class of the first row
        #print("it's fine")

        NInt=int(test[0])
        LenList=int(len(test)-1)
        #print(LenList)
        self.assertEqual(NInt,LenList)
        # Then we test the equality of the number in the first row with all interactions


        with open(FirstLine_Wrong, "r") as interaction_file:
            first_line = int(interaction_file.readline(1))  # Gets the first line
            lines_num = count.interaction_file()
            if first_line == (lines_num-1):
                print("number of interactions is correct")
            else:
                print("number of interactions is false")

        is_interaction_file("FirstLine_Wrong.txt")

        """
        This test verify the good functioning of the read_interaction file
        """
        file = Path("toy_example.txt")
        #We store our test file in the variable file

        """
        Is there the right number of column ?
        """
        column=main.read_interaction_file_list(file)[1]
        #print(type(column))
        data=len(column.split())
        self.assertEqual(data,2)
        #strip=len(column.strip())
        print("No problem occured")
unittest.main()

read_interaction_file("~/data/toy_example.txt")
