#! /usr/bin/env python3

import os
import glob

myProteome = "/home/fabien/Desktop/M2_Bioinfo/BS2/bs2/Week_5/Human_HighQuality.txt"
humanProteome = "/home/fabien/Desktop/M2_Bioinfo/BS2/bs2/Week_5/data/uniprot_proteome_Human.txt"
dataDir="/home/fabien/Desktop/M2_Bioinfo/BS2/bs2/Week_5/data"
solutionDir="/home/fabien/Desktop/M2_Bioinfo/BS2/bs2/Week_5/solution"


generatedCode = """#! /usr/bin/env python3

import unittest
import mainW5


class testDomainComponentGenerated(unittest.TestCase):"""
usecaseFilePaths = glob.glob(dataDir + os.path.sep + "*.txt")

for currentUsecaseFilePath in usecaseFilePaths:
	solutionFilePath = currentUsecaseFilePath.replace(myProteome, solutionDir)
	if os.path.exists(solutionFilePath):
		with open(solutionFilePath) as solutionFile:
			currentSolution = solutionFile.read().strip()
			solutionFileName = currentUsecaseFilePath.replace(dataDir + os.path.sep, "").replace(".txt", "")
			generatedCode +="""

	def test_""" + solutionFileName + """(self):"""

		generatedCode +="""

		data = mainW5.Interaction_file(""" + '"' + myProteome + '"'
		generatedCode +=""","""+'"'+ humanProteome + '"' """)"""
		dictExtendNameCurrentSolutionPath = solutionDir + "/"+ solutionFileName + "_x_link_Uniprot.txt"
		if os.path.exists(dictExtendNameCurrentSolutionPath):
			with open(dictExtendNameCurrentSolutionPath) as solutionFile:
				currentSolution = solutionFile.read().strip()
				generatedCode +="""
		self.assertEqual(type(data.dict_graphe["MPIP1_HUMAN"]), """ + currentSolution + """, "Should be """ + currentSolution + """")"""

		domainProteinNameCurrentSolutionPath = solutionDir + "/"+ solutionFileName + "_get_protein_domains.txt"
		if os.path.exists(domainProteinNameCurrentSolutionPath):
			with open(domainProteinNameCurrentSolutionPath) as solutionFile:
				currentSolution = solutionFile.read().strip()
				generatedCode +="""
		self.assertEqual(data.get_protein_domains("1433B_HUMAN"), """ + currentSolution + """, "Should be """ + currentSolution + """")"""

generatedCode += """

if __name__ == '__main__':
    unittest.main()
"""

with open("generatedProteinDomainInteraction.py", "w") as generatedFile:

	generatedFile.write(generatedCode)
