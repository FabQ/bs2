#! /usr/bin/env python3

import os
import glob

dataDir = "~/data"
solutionDir = "~/solution"
testDir = "/home/fabien/Desktop/M2_Bioinfo/BS2/bs2/test"

generatedCode = """#! /usr/bin/env python3

import unittest
import connexe

class testconnexeGenerated(unittest.TestCase):"""
usecaseFilePaths = glob.glob(dataDir + os.path.sep + "*.txt")

for currentUsecaseFilePath in usecaseFilePaths:
	solutionFilePath = currentUsecaseFilePath.replace(dataDir, solutionDir)
	if os.path.exists(solutionFilePath):

		with open(solutionFilePath) as solutionFile:
			currentSolution = solutionFile.read().strip()
			solutionFileName = currentUsecaseFilePath.replace(dataDir + os.path.sep, "").replace(".txt", "")
			generatedCode +="""

	def test_""" + solutionFileName + """(self):"""

		generatedCode +="""

		data = connexe.Connexe(""" + '"' + currentUsecaseFilePath + '"' + """)"""

		extractCurrentSolutionPath = solutionDir + "/" + solutionFileName + "_extractCC.txt"

		if os.path.exists(extractCurrentSolutionPath):
			with open(extractCurrentSolutionPath) as solutionFile:
				currentSolution = solutionFile.read().strip()
				generatedCode +="""
		self.assertEqual(data.extractCC("A"), """ + currentSolution + """, "Should be """ + currentSolution + """")"""

		densityCurrentSolutionPath = solutionDir + "/" + solutionFileName + "_density.txt"
		if os.path.exists(densityCurrentSolutionPath):
			with open(densityCurrentSolutionPath) as solutionFile:
				currentSolution = solutionFile.read().strip()
				generatedCode +="""
		self.assertEqual(data.density(), """ + currentSolution +""", "Should be """ + currentSolution + """")"""

		countCurrentSolutionPath = solutionDir + "/" + solutionFileName + "_countCC.txt"
		if os.path.exists(countCurrentSolutionPath):
			with open(countCurrentSolutionPath) as solutionFile:
				currentSolution = solutionFile.read().strip()
				generatedCode +="""
		self.assertEqual(type(data.countCC()), """ + currentSolution + """, "Should be """ + currentSolution + """")
"""
usecaseFilePaths = glob.glob(testDir + os.path.sep + "*.txt")
for currentUsecaseFilePath in usecaseFilePaths:
	if os.path.exists(currentUsecaseFilePath):
		generatedCode +="""

		data = connexe.Connexe(""" + '"' + currentUsecaseFilePath + '"' + """)"""
		with open(currentUsecaseFilePath):
			generatedCode +="""
	self.assertEqual(type(data.density()), """ + float +""", "Should be """ + currentSolution + """")
	self.assertEqual(type(data.countCC()), """ + tuple +""", "Should be """ + currentSolution + """")
	self.assertEqual(type(data.list), """ + list +""", "Should be """ + currentSolution + """")
	self.assertEqual(type(data.clustering()), """ + float +""", "Should be """ + currentSolution + """")
	"""

generatedCode += """

if __name__ == '__main__':
    unittest.main()
"""

with open("generatedTestConnexe.py", "w") as generatedFile:

	generatedFile.write(generatedCode)
