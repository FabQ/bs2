import unittest
from pathlib import Path
import connexe

class TestFunction(unittest.TestCase):
    def   test_extractCC(self):
        """
        Does extractCC function extracts the right connected component ?
        @Parameter  file
        @Return : True if it's the right number
        """
        toy=connexe.extractCC("D")
        self.assertEqual(toy,['D', 'B', 'E', 'F', 'A', 'C'])

unittest.main()
