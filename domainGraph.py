import numpy as np
import urllib.request
import pickle
import os
import main
from collections import Counter,OrderedDict
from matplotlib import pyplot as plt
from tqdm import tqdm
import networkx as nx
import csv


class DomainGraph(main.Interaction_file) :
    # Attributes
    co_occurence_graphe={}
    dict_graphe={}

    def __init__(self,filepath,uniprotDic):
        self.co_occurence_graphe=self.__open_dic(filepath)
        self.dict_graphe=self.__open_dic(uniprotDic)


    def __open_dic(self,filepath):
        """
        Function to init the dictionary entities created with pickle functions

        @key : Domain name
        @value : Every domain present with the @key in a minimum of one protein
        """
        with open(filepath, 'rb') as f:
            dict = pickle.load(f)
        f.close()
        return dict

    def see_co_occurence_graph(self,number):
        dict_return={}
        count_co_occurence = 0
        count_co_occurence_domain=0
        for domain in self.co_occurence_graphe.keys():
            for co_occurence_domain in self.co_occurence_graphe[str(domain)].keys():
                co_occurence_number = self.co_occurence_graphe[str(domain)][str(co_occurence_domain)]

                if co_occurence_number >= number :
                        dict_return[domain] = {co_occurence_domain:co_occurence_number}
                        count_co_occurence += dict_return.get(domain, {}).get(co_occurence_domain)
                        count_co_occurence_domain +=1
        print("Dict created with interaction with a minimum of %s contains %s domains with a total of %s interactions" %(number,count_co_occurence_domain,count_co_occurence))
        return(dict_return)

    def ls_proteins(self):
        """
        Return the list of non redundant proteins
        """
        list=self.co_occurence_graphe.keys()
        return list

    def ls_domains_n(self,n):
        """
        Return the name of domains who appear more than n times
        @parameter : n
        @return : list of domains nammed nDomainList
        """
        #Initialization
        domainList=[]
        nDomainList=[]
        # Recovery of all domains from the nested interaction dictionary
        for co_occurence_dict in self.co_occurence_graphe.values() :
            for domain,number in co_occurence_dict.items():
                if number > n :
                    nDomainList.append(domain)
                domainList.append(domain)
        return nDomainList

    def top_occurence(self,n):
        """
        Find the n domains with the highest number of neighbors
        @parameter n : top number
        """
        dict={}
        domain_list = []
        for dom,co_occ in self.co_occurence_graphe.items():
               dict[dom] = len(co_occ)
        sort_dict = sorted(dict.items(), key=lambda x: x[1], reverse=True)[:n]
        for domain in sort_dict:
            domain_list.append(domain[0])
        return domain_list

    def low_occurence(self,n):
        """
        Find the n domains with the lowest number of neighbors
        @parameter n : top number
        """
        dict={}
        domain_list = []
        for dom,co_occ in self.co_occurence_graphe.items():
               dict[dom] = len(co_occ)
        sort_dict = sorted(dict.items(), key=lambda x: x[1])[:n]
        for domain in sort_dict:
            domain_list.append(domain[0])
        return domain_list


    def co_occurrence(self,dom_x,dom_y):
        """
        Count the number of co-occurence of the domain x (dom_x) and the domain y (dom_y) in the Proteome
        @return : int : counter of co_occurence
        """
        return self.co_occurence_graphe[dom_x][dom_y]

    def __nbDomainsByProteinDistribution(self):
        """
        Distribution of the protein numbers with a domain number in a dictionnary
        @return : Dictionary
            @key : domain number associated with proteins
            @value : number of proteins which contains this number
        """
        #Initialization
        count0=0
        list=[]
        # Recuperation of the domain lists in nested dictionary of each protein in list
        for protein in self.dict_graphe.values():
            # Count if the protein has no domain
            if not protein["domains"]:
                count0 +=1
            list.append(len(protein["domains"]))
        # A counter class is settled to count the domain number per protein
        count_dict = Counter(list)
        # The 0 domain protein is added
        count_dict[0] = count0
        # Ordering the dictionary
        ordered_count_dict = OrderedDict(sorted(count_dict.items()))
        return ordered_count_dict

    def histNbDomainsByProteinDistribution(self):
        """
        Draw the histogram associated with the nbDomainsByProteinDistribution function
        @nbDomainsByProteinDistribution

        Drawing method :
        @numpy
        @matplotlib.bar()
        """
        # Set parameters necessary for the histogram drawing by plt
        y=list(dict(self.__nbDomainsByProteinDistribution()).values())
        x=list(dict(self.__nbDomainsByProteinDistribution()).keys())

        # Draw the graph with appropriate legend
        plt.bar(x,y)
        plt.title("Distribution of the protein numbers with the associated domain number")
        plt.xlabel("Domain number")
        plt.ylabel("Number of proteins")
        plt.show()

    def __nbProteinsByDomainDistribution(self):
        """
        Distribution of the domain number associated with the protein numbers in a dictionnary
        Switch keys and values from @nbDomainsByProteinDistribution
        @return : Dictionary
            @key : number of proteins which contains this number
            @value : domain number associated with proteins

        """
        # Set a new dictionnary and fill with switched keys and values from the return of @nbDomainsByProteinDistribution()
        dict={}
        previous_dict=self.__nbDomainsByProteinDistribution()
        for keys in previous_dict.keys():
            dict[previous_dict[keys]]=keys
        return dict

    def histNbProteinsByDomainDistribution(self):
        """
        Draw the histogram associated with the nbProteinsByDomainDistribution function
        @nbDomainsByProteinDistribution

        Drawing method :
        @numpy
        @matplotlib.bar()
        """
        # Set parameters necessary for the histogram drawing by plt
        x=list(dict(self.__nbProteinsByDomainDistribution()).values())
        y=list(dict(self.__nbProteinsByDomainDistribution()).keys())

        # Draw the graph with appropriate legend
        plt.bar(x,y)
        plt.title("Distribution of the protein numbers with the associated domain number")
        plt.xlabel("Domain number")
        plt.ylabel("Number of proteins")
        plt.show()

    def __nbProteinsByDomainName(self):
        """
        Distribution of the protein numbers associated with a domain name
        @return : Dictionary
            @key : domain name
            @value : protein numbers associated
        """
        dict={}
        for i in self.dict_graphe.values() :
            for domain in i.get('domains'):
                if domain not in dict.keys():
                    dict[domain]=1
                else :
                    dict[domain]+=1
        return dict

    def histNbProteinsByDomainName(self,threshold=60):
        """
        Draw a graph bar of the protein numbers associated with a domain name greater or equal to the @parameter : threshold
        @parameter : treshold : by default treshold is settled to 60 to have a good visibility
        @nbProteinsByDomainName
        """
        #x : list of domain names
        x=[]
        #y : list of protein numbers associated with a domainName
        y=[]
        for domainName in self.__nbProteinsByDomainName().keys():
            number_associated = self.__nbProteinsByDomainName()[domainName]
            # Checking if the protein numbers are greater or equal than the @threshold
            if  number_associated >= threshold :
                x.append(domainName)
                y.append(number_associated)

        # Draw the graph with appropriate legend
        plt.bar(x,y)
        plt.title("Only domains linked with a minimum of %i protein numbers are showed" % threshold,size=12)
        plt.suptitle("Protein numbers associated with a domain name",size=15)
        plt.ylabel("Number of proteins")
        plt.xlabel("Domain name")
        plt.xticks(rotation=45)
        plt.show()


    def matplolib_visualization(self,threshold=60):
        """
        Draw a co-occurence graph with matplotlib
        Co-occurence score needs to be greater or equal to the @parameter : threshold
        @matplotlib
        """
        #Collection creation
        domain_list=list(self.co_occurence_graphe.keys())
        G = nx.Graph()
        for domain in self.co_occurence_graphe.keys():
            for other_domain in self.co_occurence_graphe[str(domain)].keys():
                co_occurence_number = self.co_occurence_graphe[str(domain)][str(other_domain)]
                if co_occurence_number >= threshold :
                    G.add_node(domain)
                    G.add_node(other_domain)
                    G.add_edge(domain,other_domain, weight=co_occurence_number)

        # Create positions of all nodes and save them
        pos = nx.spring_layout(G)
        # Draw the graph according to node positions
        nx.draw(G, pos, with_labels=True)
        # Create edge labels
        labels = nx.get_edge_attributes(G,'weight')
        # Draw edge labels according to node positions
        nx.draw_networkx_edge_labels(G, pos,edge_labels=labels)
        plt.show()
        return(G)

    def cytoscape_format(self,threshold,fileName):
        """
        Draw a co-occurence graph with cytoscape
        Co-occurence score needs to be greater or equal to the @parameter : threshold
        @matplotlib
        @output : .csv file
        WARNING : you need to implement the csv file in cytoscape by yourself
        """
        with open('%s.csv'%fileName,'w', newline='') as f:
            filepath = os.path.abspath(f.name)
            fieldNames=['node1','interaction','node2','weight']
            write = csv.writer(f)
            write.writerow(fieldNames)
            for domain in self.co_occurence_graphe.keys():
                for co_occurence_domain in self.co_occurence_graphe[str(domain)].keys():
                    co_occurence_number = self.co_occurence_graphe[str(domain)][str(co_occurence_domain)]
                    if co_occurence_number >= threshold :
                        write.writerow([domain,'combined_score',co_occurence_domain,co_occurence_number/2])
        f.close()
        return("Csv file for cytoscape has been created : %s" %filepath )

    def density(self):
        """
        The density is the number of observed edges versus the theoric edges
        @return float : density number
        """
        count=0
        for domain in self.co_occurence_graphe.keys():
            for other_domain in self.co_occurence_graphe[str(domain)].keys():
                count+=1
        return(count/(len(self.co_occurence_graphe.keys())*(len(self.co_occurence_graphe.keys()))-1))

data = DomainGraph("/home/fabien/Desktop/M2_Bioinfo/BS2/bs2/data/co_occurence_graph.pkl","/home/fabien/Desktop/M2_Bioinfo/BS2/bs2/data/uniprotDic.pkl")
print(data.matplolib_visualization())
