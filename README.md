# Biological Networks Project

## Introduction
This project aims to analyze gene or domain networks from a given input interaction file.

**Goal** : Working with different file types to analyze Gene or Domain Networks.
Each class has a given prerequisite input file to work with, please be careful to avoid errors.

4 classes are provided in this project :
- Interaction_file : Observe interaction file
- Connexe : Adding connected component functions to have a better understanding of the gene networks
- Proteome : Prepare to domain functions
- domainGraph : Visualization of domain co_occurence graphs

## Project installation
```bash
git clone https://gitlab.com/FabQ/bs2
```

## Interaction_file class: Managing the input basic file

### Functions
- Basic functions to observe the data : get_vertices, count_vertices, count_edges, get_degree, get_max_degree, count_degree
- Graphical simple visualization can be done with : histogram_degree functions
### Input file
Input : a gene interaction file like the toy example provided : ***~/data/toy_example.txt***
- First line : **the interaction number**
- And all interactions, one by one :
  - **A** interacts with **B**
  - **A** also interacts with **C**
  - ...

### Unit test
  Test functions and your file with unit test function :
  ```
  python ./test/test_read_interaction.py
  python ./test/test_read_interaction2.py
  ```
## Connexe: Find operational components
A connected component is a graph unit composed by elements that interacts with at least one or more elements.
In our toy example :
```
A, B and C are in the same composed component, because B and C interacts with A
```
### Functions
- Functions to observes connected component : extractCC, computeCC, countCC
- Write results in outfile : writeCC
- Statistical result : density and clustering
### Input file
Same as Interaction_file class

### Unit test
  Test functions and your file with unit test function :
  ```
  python ./test/test_connexe.py
  python ./test/Unittest-generator.py "UnitTestName"
  python ./test/ "UnitTestName"
  ```

## Proteome: Prepare to work with domains from gene interaction file

### Input file: Proteome recuperation from a uniprot file
You want to work on domains ?

Enter your gene name list in : https://www.uniprot.org/uploadlists  
Input :
- Downloaded uniprot gene from uniprotID
- Gene interaction file
### Functions
- Create a interaction file with domains : x_link_domains (Pay attention : this function is very long to compute)
- Verify the import : uniprotID_Import
- Create the DomainGraph input file : generate_weighted_cooccurrence_graph

### Unit test
  Test functions and your file with unit test function :
  ```
  python ./test/Unittest-generator_domainComponent.py "UnitTestName"
  python ./test/"UnitTestName"
  ```
## DomainGraph: Visualization of a weighted co-ocurence graph
Domains generated from the Proteome class can be used to create graphs to check the co-occurence between domains in proteins.

A strong co-occurence score can reveal a dependency between this two domains to fulfill a biological function.

### Input file
generate_weighted_cooccurrence_graph output stored in a pickle file

### Functions
- Interact with domain graph : ls_proteins, ls_domains_n, co_occurrence,density see_co_occurence_graph
- Graphical visualization of domains :
  - histNbDomainsByProteinDistribution
  - histNbProteinsByDomainDistribution
  - histNbProteinsByDomainName
  - direct visualization : matplolib_visualization
  - Better visualization with cytoscape : cytoscape_format

### Unit test
Test functions and your file with unit test function :

    python ./test/Unittest-generator_proteinDomains.py "UnitTestName"
    python ./test/"UnitTestName"
